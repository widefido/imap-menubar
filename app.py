# -*- coding: utf-8 -*-
from __future__ import print_function

import objc
from Foundation import *
from AppKit import *
from PyObjCTools import NibClassBuilder, AppHelper

from ProcImap.Utils.MailboxFactory import MailboxFactory
from ProcImap.Utils.Server import summary, display

import os
import re
import sys
import threading
import time

try:
    import rules
    if not hasattr(rules, "match"):
        raise ImportError()
    match = rules.match
except ImportError:
    def match(*args, **kwargs):
        return True


status_images = {"idle":os.path.join(os.getcwd(), "mail.png")}

start_time = NSDate.date()


class Timer(NSObject):
    images = {}
    statusbar = None
    state = "idle"
    count = 0

    def applicationDidFinishLaunching_(self, notification):
        """
        Our application has finished launching, prepare the app status bar menu, 
        timer, images, and check logic. 

        """
        statusbar = NSStatusBar.systemStatusBar()
        
        # Create the statusbar item
        self.statusitem = statusbar.statusItemWithLength_(NSVariableStatusItemLength)
        
        # Load all images
        for key in status_images.keys():
            self.images[key] = NSImage.alloc().initByReferencingFile_(status_images[key])
        
        # Set initial image
        self.statusitem.setImage_(self.images["idle"])
        
        # Let it highlight upon clicking
        self.statusitem.setHighlightMode_(1)
        
        # Set a tooltip
        self.statusitem.setToolTip_("Notify")


        # Build a very simple menu
        self.menu = NSMenu.alloc().init()

        # Sync event is bound to sync_ method
        #menuitem = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Check Mail...", "sync:", "")
        #self.menu.addItem_(menuitem)

        ## Separator
        #separator = NSMenuItem.separatorItem()
        #self.menu.addItem_(separator)

        # Quit event
        menuitem = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Quit", "terminate:", "")
        self.menu.addItem_(menuitem)

        # Bind it to the status item
        self.statusitem.setMenu_(self.menu)

        # Get the timer going
        self.timer = NSTimer.alloc().initWithFireDate_interval_target_selector_userInfo_repeats_(start_time, 5.0, self, "tick:", None, True)
        NSRunLoop.currentRunLoop().addTimer_forMode_(self.timer, NSDefaultRunLoopMode)

        # Prepare the mail loop
        self.t, self.l = check_mail_loop()
    

    def tick_(self, notification):
        print(".", end="")
        if self.l and "count" in self.l:
            self.count = self.l["count"]

        title = "" if not self.count else " {0}".format(self.count)
        self.statusitem.setTitle_(title)

        sys.stdout.flush()


def check_mail():
    conf = os.path.join(os.getcwd(), "mailboxes.conf")
    mailboxes = MailboxFactory(conf)

    inboxes = {}

    for name in mailboxes.list():
        mailbox = mailboxes[name]

        unseen = mailbox.get_unseen_uids()
        if not unseen:
            continue

        if name not in inboxes:
            inboxes[name] = []

        for uid in unseen:
            fields = mailbox.get_fields(uid, "From Subject Content-Type")
            sender = fields['From']
            subject = fields['Subject']
            
            if not match(name, sender, subject):
                continue

            inboxes[name].append((uid, sender, subject))

    count = sum(len(messages) for name, messages in inboxes.items())

    return inboxes, count


def check_mail_loop(interval=300):
    def _loop(local):
        while True:
            sys.stdout.flush()
            inboxes, count = check_mail()
            local["inboxes"] = inboxes
            local["count"] = count

            if count:
                for name, messages in inboxes.items(): 
                    print()
                    print("{0}:".format(name))
                    for i, message in enumerate(messages):
                        print("{0}. {1}".format(i + 1, message))
            time.sleep(interval)

    loop_local = {}
    t = threading.Thread(target=_loop, args=(loop_local,))
    t.start()
    return t, loop_local


if __name__ == "__main__":
    app = NSApplication.sharedApplication()
    delegate = Timer.alloc().init()
    app.setDelegate_(delegate)
    AppHelper.runEventLoop()
