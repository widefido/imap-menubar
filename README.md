# imap-menubar
A really simple experiment of an OS X menu bar utility for displaying the 
number of unread messages in an imap mailbox that matches a certain set of 
rules.

## Dependencies

- python
    - PyObjC
    - ProcImap

## Install (using virtualenv)

    git clone https://bitbucket.org/widefido/imap-menubar.git
    cd imap-menubar
    virtualenv env
    env/bin/pip install -U -r requirements.txt

## Running

    env/bin/python app.py

## Mailboxes.conf

Mailboxes to check for new mail are configured in an INI format. You can specify
multiple mailboxes to check by adding more than one [account name] header.

Example:

    [account_name]
    type = IMAP
    mailbox = INBOX
    server = imap.gmail.com
    username = somebody@gmail.com
    password = somebodyspasswordhere
    ssl = True

## Rules.py

Rules are defined within a rules.py file. Inside this file, define a function
named "match" that accepts message data and returns True or False if the message
matches. 

Example:

    def match(account_name, sender, subject, *args, **kwargs):
        sender = sender.lower()
        subject = subject.lower()
        return (
            ("mom" in sender) or
            ("important" in subject)
        )

## TODO

- hook into the OS X keychain for account passwords and/or prompt for password
- display the accounts/messages that match within the menubar menu.
- link to the email client/web page to read the messages.
- growl? notification center?

